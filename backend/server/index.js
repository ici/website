const express = require('express'); /* ici j'importe toutes mes librairies npm installé avec npm i exemple que je stocke dans des constantes pour pouvoir les réutiliser */

const bodyParser = require('body-parser');

const cors = require('cors');

const app = express();

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });

app.use(bodyParser.json());
app.use(cors());

const posts = require('./routes/api/getdistinctvalues'); /* ici je créer mes constantes ou je stocke mes pages back end  */
const recup_value = require('./routes/api/recup_value');
const sigma = require('./routes/api/sigma');
const graph_name = require('./routes/api/graph_name');
const v_code = require('./routes/api/v_simulateur') ; 
const info_pop = require('./routes/api/info_pop') ; 

app.use('/api/recup_value', recup_value); /* ici je créer mes routes pour mes requêtes back end */
app.use('/api/getdistinctvalues', posts);
app.use('/api/sigma', sigma);
app.use('/api/graph_name', graph_name);
app.use('/api/v_simulateur', v_code) ;
app.use('/api/info_pop', info_pop) ;  

const port = process.env.PORT || 5000; /* ici je définit un port local pour mon serveur back end */

app.listen(port, () => console.log(`Server started on port ${port}`));  /* ici je lance le serveur sur le port choisi précedemment */

