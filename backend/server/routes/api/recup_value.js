const express = require('express');  /* ici j'importe toutes mes librairies npm installé avec npm i exemple */
const mongodb = require('mongodb');
const cors = require('cors');
const path = require('path');
const app = express();
const router = express.Router();
var expressVue = require("express-vue");

app.use(cors());  /* utilisation du package CORS pour éviter les erreur du à la connexion entre les 2 lien */
/*  require('../../../../src/components/Formulaire.vue');  */  /* ici j'ai essayé d'importer la valeur de item.probability directement depuis le front mais ca n'a pas marché */

/* router.get('http://localhost:8080/', (req, res) => {
    const probabilité = {utilisateur};
    res.send(global.findOne({"param_fct.mu_prob": {utilisateur}}));
}) */ /* ici j'ai essayé de lier mon router avec le front afin de récupérer la valeur mais il ne trouvait pas l'adresse du site */


router.post('/',async (req, res) => {  /* ici je me connecte à ma base de données et j'essaie d'associer la valeur item.probability au champ mu_prob mais ca ne fonctionne pas */
    const reports = await loadPostsCollection();
    const proba = req.body.probability;
    const dist = req.body.dist ; 
    const time = req.body.time ; 
    const indic = req.body.indic ; 
    const bool_global = req.body.bool_global ; 
    const bool_occup = req.body.bool_occup ; 
    const bool_age = req.body.bool_age ; 
    const class_age = req.body.age ; 
    const occup = req.body.occup ; 
    const flux = req.body.flux ; 
    var indic_json = null ; 
    var report = null ; 
    var population = null ; 

    if(bool_global) {
        population = 'Global' ; 
    } else {
        if(bool_occup) {
            if(flux == "Total") {
                population = `${occup}` ; 
            } else {
                population = `${flux}_${occup}` ; 
            } ; 
        } 
        if(bool_age) {
            switch(class_age) {
                case '0-2 ans' :
                    population = "0_2" ;
                    break;  
                case '3-5 ans' :
                    population = "3_5" ;
                    break; 
                case '6-10 ans':
                    population = "6_10" ;
                    break;
                case '11-17 ans':
                    population = "11_17" ;
                    break; 
                case '18-24 ans':
                    population = "18_24" ;
                    break;
                case '25-39 ans':
                    population = "25_39" ;
                    break; 
                case '40-54 ans':
                    population = "40_54" ;
                    break; 
                case '55-64 ans':
                    population = "55_64" ;
                    break;
                case '65-79 ans':
                    population = "65_79" ;
                    break;
                case "+ 80 ans":
                    population = "80p" ;
                    break;
            }
        } 
    } 

    switch(indic) {
        case 'Sains': 
            indic_json = "susceptible" ; 
            break; 
        case 'Exposés': 
            indic_json = "exposed" ; 
            break;
        case 'Contaminants': 
            indic_json = "infected" ; 
            break;
        case 'Hospitalisés': 
            indic_json = "hospitalized" ; 
            break; 
        case 'Soins intensifs': 
            indic_json = "icu" ; 
            break; 
        case 'Décès': 
            indic_json = "death" ; 
            break ; 
        case 'Guéris': 
            indic_json = "recovered" ; 
            break ; 
    }

    if(bool_global) {
        report = await reports.findOne({"param_fct.mu_prob": proba,"param_fct.dist_min_menage": dist,"param_fct.time_min_menage":time },{[`${population}.${indic_json}`]:1}) ; 
        res.send(await report[`${population}`][`${indic_json}`]) ; 
    } else {
        if(bool_occup){
            report = await reports.findOne({"param_fct.mu_prob": proba,"param_fct.dist_min_menage": dist,"param_fct.time_min_menage":time },{[`PerGroup.${indic_json}.${population}`]:1}) ; 
            res.send(await report['PerGroup'][`${indic_json}`][`${population}`]) ; 
        }
        if(bool_age){
            report = await reports.findOne({"param_fct.mu_prob": proba,"param_fct.dist_min_menage": dist,"param_fct.time_min_menage":time },{[`PerAge.${indic_json}.${population}`]:1}) ; 
            res.send(await report['PerAge'][`${indic_json}`][`${population}`]) ;
        }
    }


 /* console.log((await global.findOne({"param_fct.mu_prob": (0.05)}).exec()));*/ /* ici je test avec exec dans la console pour voir si ça fonctionne ou non */
/*  console.log((await global.findOne({"param_fct.mu_prob": (0.05)}).exec())); */
  /* res.send(await global.findOne({"param_fct.mu_prob": 0.05})); */ /* cette requête fonctionne grâce à une valeur rentrée par défaut il suffit de la décommenter et de commenter celle avec req.body.probability */
  /* res.view('../../../../src/components/Formulaire.vue', {utilisateur: { probabilité: global.findOne}})*/ /* ici j'essaie de récupérer la valeur de item.probability directement depuis le front mais ca ne fonctionne pas */
/* expressVue.use(express, expressVueOptions).then(() => {
    res.renderVue('../../../../src/components/Formulaire.vue', {utilisateur: { probabilité: global.findOne}})
}); */


 /* res.send(await global.find({}).toArray());*/

});


async function loadPostsCollection() {  /* ici je me connecte à la base de données en local */
    const client = await mongodb.MongoClient.connect
    ('mongodb://127.0.0.1:27017/test_ici', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    return client.db('test_ici').collection('simulation'); /* ici nicolas correpond au nom de ma base de données et test au nom de ma collection */
}

module.exports = router;
