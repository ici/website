
const express = require('express');  /* ici j'importe toutes mes librairies npm installé avec npm i exemple */
const mongodb = require('mongodb');
const cors = require('cors');
const router = express.Router();
const path = require('path');
var expressVue = require("express-vue");
/*  require('../../../../src/components/Formulaire.vue');  */  /* ici j'ai essayé d'importer la valeur de item.probability directement depuis le front mais ca n'a pas marché */
app.use(cors());  /* utilisation du package CORS pour éviter les erreur du à la connexion entre les 2 lien */

router.post('/',async (req, res) => {  /* ici je me connecte à ma base de données et j'essaie d'associer la valeur item.probability au champ mu_prob mais ca ne fonctionne pas */
    const global = await loadPostsCollection();
    const proba = req.body.probability;
    console.log(proba);
    res.send(await global.findOne({"param_fct.mu_prob": proba},{"Global.susceptible":1}));
 /* console.log((await global.findOne({"param_fct.mu_prob": (0.05)}).exec()));*/ /* ici je test avec exec dans la console pour voir si ça fonctionne ou non */
/*  console.log((await global.findOne({"param_fct.mu_prob": (0.05)}).exec())); */
  /* res.send(await global.findOne({"param_fct.mu_prob": 0.05})); */ /* cette requête fonctionne grâce à une valeur rentrée par défaut il suffit de la décommenter et de commenter celle avec req.body.probability */
  /* res.view('../../../../src/components/Formulaire.vue', {utilisateur: { probabilité: global.findOne}})*/ /* ici j'essaie de récupérer la valeur de item.probability directement depuis le front mais ca ne fonctionne pas */
/* expressVue.use(express, expressVueOptions).then(() => {
    res.renderVue('../../../../src/components/Formulaire.vue', {utilisateur: { probabilité: global.findOne}})
}); */


 /* res.send(await global.find({}).toArray());*/

});


async function loadPostsCollection() {  /* ici je me connecte à la base de données en local */
    const client = await mongodb.MongoClient.connect
    ('mongodb://127.0.0.1:27017/test_ici', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    return client.db('test_ici').collection('simulation'); /* ici nicolas correpond au nom de ma base de données et test au nom de ma collection */
}

module.exports = router;
