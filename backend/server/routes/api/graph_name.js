const express = require('express'); /* ici j'importe toutes mes librairies npm installé avec npm i exemple que je stocke dans des constantes pour pouvoir les réutiliser */
const mongodb = require('mongodb');
const router = express.Router();
const app = express();
const cors = require('cors');
const path = require('path');
require ('../../index');

app.use(cors());  /* utilisation du package CORS pour éviter les erreur du à la connexion entre les 2 lien */


router.get('/',async (req, res) => {  /* ici je me connecte à ma base de données et j'associe toutes les différentes valeurs du champ mu_prob à la sélection de la probabilité de transmission  */
    const posts = await loadPostsCollection();
    var graph = await posts.distinct("param_simu_gen.graph_name") ; 

    if(graph == "outSaintVic-v0.3") {
        graph = ["Paris V - Jussieu/St-Victor"] ; 
    }

    const param = {graph_name: graph ,nb_pop: await posts.distinct("param_simu_gen.nb_pop")};

    res.send(param);
});


async function loadPostsCollection() {  /* ici je me connecte à la base de données en local */
    const client = await mongodb.MongoClient.connect
    ('mongodb://127.0.0.1:27017/test_ici', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    return client.db('test_ici').collection('simulation'); /* ici nicolas correpond au nom de ma base de données et test au nom de ma collection */
}

module.exports = router;
