const express = require('express'); /* ici j'importe toutes mes librairies npm installé avec npm i exemple que je stocke dans des constantes pour pouvoir les réutiliser */
const mongodb = require('mongodb');
const router = express.Router();
const app = express();
const cors = require('cors');
const path = require('path');
require ('../../index');

app.use(cors());  /* utilisation du package CORS pour éviter les erreur du à la connexion entre les 2 lien */

router.get('../../../../src/components/Formulaire.vue', (req, res,next)=>{ /* tentative de lien entre les valeurs select et les données sigma,mu et deb avec les routes */
  res.send('const sigma = document.querySelector("#sigma-select");const proba = document.querySelector("#proba-select");const deb = document.querySelector("#deb-select");'); res.sendFile(path.join(__dirname, 'views', 'add-   Formulaire.vue'));
 });


 router.post('/', (req, res, next)=>{  
    const sigma = req.body.quartier;
    const mu = req.body.probability;
    const deb = req.body.choix;
    res.send('<h1>'+req.body.choix+'</h1>');
 });

router.get('/',async (req, res) => {  /* ici je me connecte à ma base de données et j'associe toutes les différentes valeurs du champ mu_prob à la sélection de la probabilité de transmission  */
    const posts = await loadPostsCollection();
    const param = {mu: await posts.distinct("param_fct.mu_prob"),dist: await posts.distinct("param_fct.dist_min_menage"), time: await posts.distinct("param_fct.time_min_menage") };
    res.send(param);
   
/*  res.send(await posts.find({}).toArray());*/ /* cette requête permet de renvoyer toutes les données de la base de données  */

});


router.post('/', async (req, res) =>  { /* cette fonction sert à stocker les données pour vérifier si on récupère bien les bonnées données par exemple pour item.probabilité  */
    const posts = await loadPostsCollection(); /* on peut ensuite bien sûr les supprimer à la main */
    await posts.insertOne({
        quartier: req.body.quartier,
        population: req.body.population,
        choix: req.body.choix,
        probabilité: req.body.probability,
    });
    res.status(201).send();
});


async function loadPostsCollection() {  /* ici je me connecte à la base de données en local */
    const client = await mongodb.MongoClient.connect('mongodb://127.0.0.1:27017/test_ici', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    return client.db('test_ici').collection('simulation'); /* ici nicolas correpond au nom de ma base de données et test au nom de ma collection */
}

module.exports = router;
