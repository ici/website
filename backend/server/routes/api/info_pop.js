
const express = require('express');  /* ici j'importe toutes mes librairies npm installé avec npm i exemple */
const mongodb = require('mongodb');
const cors = require('cors');
const app = express();
const router = express.Router();
var expressVue = require("express-vue");

app.use(cors());  /* utilisation du package CORS pour éviter les erreur du à la connexion entre les 2 lien */

/*  require('../../../../src/components/Formulaire.vue');  */  /* ici j'ai essayé d'importer la valeur de item.probability directement depuis le front mais ca n'a pas marché */

/* router.get('http://localhost:8080/', (req, res) => {
    const probabilité = {utilisateur};
    res.send(global.findOne({"param_fct.mu_prob": {utilisateur}}));
}) */ /* ici j'ai essayé de lier mon router avec le front afin de récupérer la valeur mais il ne trouvait pas l'adresse du site */


router.post('/',async (req, res) => {  /* ici je me connecte à ma base de données et j'essaie d'associer la valeur item.probability au champ mu_prob mais ca ne fonctionne pas */
    const reports = await loadPostsCollection();
    const quartier = req.body.quartier;
    var report = null ; 
    
    /*if(quartier == ["Paris V - Jussieu/St-Victor"]) {
        quartier = ["outSaintVic-v0.3"] ; 
    } */
    console.log(quartier)
    report = await reports.findOne({"param_simu_gen.graph_name": quartier}) ; 
    const pop = {total: report["param_simu_gen"]["nb_pop"], per_occup_flux: report["PerGroup"]["nb_pop"], per_age: report["PerAge"]["nb_pop"]};
    console.log(pop.per_occup_flux) ;
    res.send(pop); 
});


async function loadPostsCollection() {  /* ici je me connecte à la base de données en local */
    const client = await mongodb.MongoClient.connect
    ('mongodb://127.0.0.1:27017/test_ici', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    return client.db('test_ici').collection('simulation'); /* ici nicolas correpond au nom de ma base de données et test au nom de ma collection */
}

module.exports = router;
