import Vue from 'vue' /* ici j'importe toutes mes bibliothèques nécessaires dans vue */
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import User from '@/components/FormUser.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  /* ici je définit la route principal lors de l'ouverture de l'application avec la page HomeView.vue */
  {
    path: '/about',
    name: 'about',
    /* ici je définit la route about pour comprendre le système de routing */
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
  {
    path: '/users',
    name: 'user',
    component: User
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
