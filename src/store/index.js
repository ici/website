import Vue from 'vue' /* cette page sert à l'utilisation de VueX je l'avais utilisé pour quelques tests */
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
